function getMyPosition() {

    window.navigator.geolocation.getCurrentPosition(function(position) {

        let infos_lat = position.coords.latitude;
        let infos_lon = position.coords.longitude;

        document.getElementById("lat").value = infos_lat;
        document.getElementById("lon").value = infos_lon;

        console.log("Position trouvée : Latitude="+position.coords.latitude+" Longitude="+position.coords.longitude);
        console.log(position.coords);

        document.getElementById("submit").innerHTML ="<button type=\"submit\" class=\"btn btn-primary\">Démarrer</button>";
    }, function(error) {
        console.log("Erreur de géolocalisation N°"+error.code+" : "+error.message);
        console.log(error);
    });
}

getMyPosition();






