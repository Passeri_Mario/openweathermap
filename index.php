<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Météo Passeri Mario</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#5690d9"/>
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="mobile-web-app-capable" content="yes"/>
        <link rel="icon" type="image/png" href="assets/icons/icon-48x48.png"/>
        <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="manifest" href="manifest.json"/>
        <link rel="apple-touch-icon" href="assets/icons/icon-192x192.png"/>
        <script type="module" src="pwabuilder-sw-register.js"></script>
    </head>
    <body class="container">

        <nav class="navbar bg-light mt-5 mb-5">
            <div class="container-fluid">
                <div class="d-flex align-items-center">
                    <img class="d-inline-block align-text-top" src="assets/icons/icon-512x512.png" alt="" width="50" height="44">
                    <a class="navbar-brand me-3 ms-3" href="/">Application Météo</a>
                </div>
            </div>
        </nav>

        <div class="card mt-5 mb-5">
            <img src="https://images.ctfassets.net/hrltx12pl8hq/6TIZLa1AKeBel0yVO7ReIn/1fc0e2fd9fcc6d66b3cc733aa2547e11/weather-images.jpg" class="card-img-top" alt="...">
            <div id="card-body" class="card-body">
                <form method="POST" action="app.php">
                    <div class="mb-3">
                        <input type="hidden" class="form-control" id="lat" name="lat">
                        <input type="hidden" class="form-control" id="lon" name="lon">
                    </div>
                    <div id="submit">
                        <div class="spinner-border text-primary" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="footer" class="alert alert-primary" role="alert">
            <p>Demo météo API - Passeri Mario</p>
            <a target="_blank" href="https://gitlab.com/Passeri_Mario/openweathermap">Lien GitLab</a>
        </div>
        <script src="assets/js/app.js"></script>
    </body>
</html>